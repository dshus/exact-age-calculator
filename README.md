#This one isn't as safe.  It's safe, sure, but... dates are hard.

https://www.youtube.com/watch?v=-5wpm-gesOY

Things that are protected against:

* Invalid Date Formatting

* Invalid Dates (mostly)

* Dates that haven't happened yet

* Leap years (probably)

Things that AREN'T protected against:

* Leap years (maybe)

* Lots of other stuff
