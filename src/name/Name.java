/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package name;

import java.awt.HeadlessException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
/**
 *
 * @author 20shustind
 */
public class Name {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String DOB = "";
        String name = "";
        int month;
        int day;
        int year;
        try {
            name = JOptionPane.showInputDialog("What is your name?");
            if (name == null){
                System.out.println("Good-bye!");
                return;
            }
            DOB = JOptionPane.showInputDialog("What is your Date of Birth? (MONTH/DAY/YEAR)");
            if (DOB == null) {
                System.out.println("Good-bye");
                return;
            }
        } catch (HeadlessException e) {
            System.err.println("An error occured.");
            return;
        }
        Pattern reg = Pattern.compile("(\\d+)\\/(\\d+)\\/(\\d+)");
        Matcher m = reg.matcher(DOB);
        if (m.matches()) {
            month = Integer.parseInt(m.group(1));
            day = Integer.parseInt(m.group(2));
            year = Integer.parseInt(m.group(3));
            String[] ids = TimeZone.getAvailableIDs(-4 * 60 * 60 * 1000);
            if (ids.length == 0) {
                System.err.println("An error occured.");
                System.exit(0);
            }
            SimpleTimeZone edt = new SimpleTimeZone(-8 * 60 * 60 * 1000, ids[0]);
            edt.setStartRule(Calendar.APRIL, 1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);
            edt.setEndRule(Calendar.OCTOBER, -1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);
            Calendar cal = new GregorianCalendar(edt);
            Calendar now = new GregorianCalendar(edt);
            cal.setLenient(false);
            cal.set(year, month-1, day);
            now.setTime(new Date());
            try {
                cal.getTime();
                now.getTime();
            } catch (Exception e) {
                System.err.println("The date is not valid.");
                return;
            }
            int dayAge = (int) TimeUnit.MILLISECONDS.toDays(now.getTimeInMillis() - cal.getTimeInMillis());
            int years = Math.abs((int)Math.floor(dayAge/365.0));
            int days = Math.abs(dayAge) % 365 + (now.YEAR % 4 == 0 ? (now.MONTH > 1 ? 1 : 0) : 0);
            System.out.println(dayAge < 0 ? "Hello, " + name + "! It seems you have not been born yet, but you will be born in " + years + " year" + (years == 1 ? "" : "s") + " and " + days + " days!" : "Hello, " + name + "! You are " + years+ " year" + (years == 1 ? "" : "s") + " and " + days + " days old.");
    }
	else
            System.err.println("No match was found. Please enter valid input.");
        return;
    }
    
}
